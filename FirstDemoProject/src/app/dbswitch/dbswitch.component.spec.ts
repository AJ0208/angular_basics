import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DBSwitchComponent } from './dbswitch.component';

describe('DBSwitchComponent', () => {
  let component: DBSwitchComponent;
  let fixture: ComponentFixture<DBSwitchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DBSwitchComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DBSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
