import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiserviceService } from '../utils/services/apiservice.service';

@Component({
  selector: 'app-dbswitch',
  templateUrl: './dbswitch.component.html',
  styleUrls: ['./dbswitch.component.css']
})
export class DBSwitchComponent implements OnInit {
  selectedValue: any;
  textBoxValue!: string ;
  dropdownOptions: any[] = []; // Initialize as empty array
  

  constructor(private apiService : ApiserviceService,
              private router: Router) { }

  ngOnInit(): void {
    this.fetchDbList();
    this. onSelectDb()
  }

  fetchDbList(): void {
    this.apiService.getDbNames().subscribe(
      (data) => {
        this.dropdownOptions = data; // Assuming the response data is an array of objects
      },
      (error) => {
        console.error('Error fetching DB list:', error);
      }
    );
  }

  
  // onSelectDb(): void {
  //   // debugger
  //   // console.log(this.selectedValue);
  //   localStorage.setItem('DbId', this.selectedValue.Id)
  //   this.apiService.getUserData().subscribe(
  //     (response) => {
  //       console.log(response);
  //     },
  //     (error) => {
  //       console.error('Error in userGet API:', error);
  //     }
  //   );


  onSelectDb(): void {
    if (this.selectedValue) {
      localStorage.setItem('DbId', this.selectedValue);
      this.apiService.getUserData().subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.error('Error in userGet API:', error);
        }
      );
    } else {
      console.error('No valid selected option');
    }
  }
  

}
  

//   }

// }


