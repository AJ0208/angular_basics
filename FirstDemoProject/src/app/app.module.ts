import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AddContactComponent } from './pages/contact/add-contact/add-contact.component';
import { ContactListComponent } from './pages/contacList/contact-list/ContactListComponent';
import {MatIconModule} from '@angular/material/icon';
import { DBSwitchComponent } from './dbswitch/dbswitch.component';
import { FormIoDemoComponent } from './test- FormIO/form-io-demo/form-io-demo.component';
import { FormioModule } from 'angular-formio';
import { FormIoRedirectionComponent } from './test- FormIO/form-io-redirection/form-io-redirection.component';

//import { DataTablesModule } from 'angular-datatables';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AddContactComponent,
    ContactListComponent,
    DBSwitchComponent,
    FormIoDemoComponent,
    FormIoRedirectionComponent
    
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatIconModule,
    FormioModule

   // DataTablesModule   //Import our DataTablesModule.
    
      
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class appModule {
}
