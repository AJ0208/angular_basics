import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/utils/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  submitted = false; // Define and initialize the submitted variable

  constructor(private formBuilder: FormBuilder,
             private route: ActivatedRoute ,
             private authenticationService: AuthenticationService,             
             private router: Router,) { }


  ngOnInit() {
    
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

   // convenience getter for easy access to form fields
   get f() { return this.loginForm.controls; }


  // onSubmit(): void {
  //   this.submitted = true; // Set submitted to true on form submission
  //   if (this.loginForm.valid){
  //     console.log('Form submitted!', this.loginForm.value);
  //     // Call your authentication service here to handle login
  //     // For example: this.authenticationService.login(this.loginForm.value);
  //   } else {
  //     console.log('Form is invalid. Please check the fields.');
  //   }
  // }

  // onSubmit(): void {
  //   this.submitted = true; // Set submitted to true on form submission
  //   if (this.loginForm.valid) {
  //     const { username, password } = this.loginForm.value;
  //     this.authenticationService.login(username, password)
  //       .subscribe({
  //         next: (response) => {
  //           // Assuming the API returns a response containing a welcome page URL
  //           const welcomePageUrl = response.welcomePageUrl;
  //           // Redirect to the welcome page
  //           this.router.navigateByUrl(welcomePageUrl);
  //         },
  //         error: (error) => {
  //           console.error('Login failed:', error);
  //         }
  //       });
  //   } else {
  //     console.log('Form is invalid. Please check the fields.');
  //   }
  // }
  
  onSubmit(): void {
    this.submitted = true; // Set submitted to true on form submission
    if (this.loginForm.valid) {
      const { username, password } = this.loginForm.value;
      this.authenticationService.login(username, password)
        .subscribe({
          next: () => {
            // Redirect to the welcome page
           // alert("welcome anjali");
            this.router.navigateByUrl('/contactList');
          },
          error: (error) => {
            alert('Login failed:'+ error);
            console.error('Login failed:', error);
          }
        });
    } else {
      console.log('Form is invalid. Please check the fields.');
    }
  }


  
}



