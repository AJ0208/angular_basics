import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';



@Component({
    selector: 'app-contact-list',
    templateUrl: './contact-list.component.html',
    styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

    contactsData: any[] = [
        { name: 'John Doe', email: 'john@example.com', state: 'California', city: 'Los Angeles', phoneNumber: '1234567890' },
        { name: 'Jane Smith', email: 'jane@example.com', state: 'New York', city: 'New York City', phoneNumber: '9876543210' },
        // Add more contacts as needed
    ];
  contactForm: any;

    constructor(private http: HttpClient,
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder // Inject FormBuilder
      ) { }

    // ngOnInit(): void {
    //     // Initialize component
    // }


    addContact() {
        // Implement logic to add contact
        this.router.navigate(['/addContact']);
    }

    viewContact(contact: any) {
        // Implement logic to view contact
    }

    // editContact(contact: any) {
    //     // Redirect to the add contact page
    //     this.router.navigate(['/addContact']); // Assuming 'addContact' is the path to your add contact page
    //   }
    editContact(contact: any) {
      // Redirect to the add contact page and pass the contact ID
      //this.router.navigate(['/addContact'], { state: { contactId: contact.id } });
       // Redirect to the add contact page and pass the contact details as route state
     this.router.navigate(['/addContact'], { state: { contactId: contact.id, contactDetails: contact } });
  }
      
    deleteContact(contact: any) {
        // Implement logic to delete contact
    }

    generateInvoice(contact: any) {
        // Implement logic to generate invoice
    }

    ngOnInit(): void {
  }
  
}
  

  

