import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.css']
})
export class AddContactComponent implements OnInit {
  contactForm!: FormGroup; // Declare contactForm property of type FormGroup
  submitted = false; // Define and initialize the submitted variable


  private loginHeader = new HttpHeaders({
    'content-type': 'application/json'
  });


  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    // Initialize the contactForm FormGroup in the ngOnInit lifecycle hook
    this.contactForm = this.formBuilder.group({
      contactType: ['patient', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      state: ['California', Validators.required],
      city: ['Los Angeles', Validators.required],
      zipCode: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      address: ['', Validators.required]
    });

    this.editContact();

  }

  onSubmit(): void {
    debugger;
    console.log(this.contactForm.value);
  }

  editContact(): void {

    this.route.paramMap.subscribe(params => {
      const contactId = '96BEB375-4C4A-4A18-8E2E-6586D5BDBD11'//params.get('contactId');
      if (contactId) {
        this.http.post<any>(`${environment.apiUrl}/api/contacttype/getContactDetails`, { contactId })
          .subscribe(response => {
            // Populate the form fields with the fetched contact details
            this.contactForm.patchValue({
              firstName: response.data.firstName,
              lastName: response.data.lastName,
              email: response.data.email,
              state: response.data.state,
              city: response.data.city,
              zipCode: response.data.zipCode,
              phoneNumber: response.data.phone,
              address: response.data.address
            });
          }, error => {
            console.error('Error fetching contact details:', error);
          });
      }
    });
  }

}


