import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormIoRedirectionComponent } from './form-io-redirection.component';

describe('FormIoRedirectionComponent', () => {
  let component: FormIoRedirectionComponent;
  let fixture: ComponentFixture<FormIoRedirectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormIoRedirectionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormIoRedirectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
