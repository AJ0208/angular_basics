import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from 'src/app/utils/services/apiservice.service';

@Component({
  selector: 'app-form-io-redirection',
  templateUrl: './form-io-redirection.component.html',
  styleUrls: ['./form-io-redirection.component.css']
})
export class FormIoRedirectionComponent implements OnInit {
  
  forms: { formName: string, createdForm: any[] }[] = [];
  selectedForm: { formName: string, createdForm: any[] } | null = null;

  constructor(private apiService: ApiserviceService) { }

  ngOnInit(): void {
    this.getForms();
  }

  getForms() {
    this.apiService.getForms().subscribe(
      (response) => {
        debugger
        console.log('Forms:', response);
        this.forms = response.data.map((form: any) => {
          return {
            formName: form.formName,
            createdForm: JSON.parse(form.createdForm)
          };
        });
      },
      (error) => {
        console.error('Error fetching forms:', error);
      }
    );
  }

  toggleFormDetails(form: { formName: string, createdForm: any[] }) {
    this.selectedForm = (this.selectedForm === form) ? null : form;
  }

  onFormSubmit(submission: any) {
    console.log('Form submitted:', submission);
    // Handle form submission logic here
  }
}