import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormIoDemoComponent } from './form-io-demo.component';

describe('FormIoDemoComponent', () => {
  let component: FormIoDemoComponent;
  let fixture: ComponentFixture<FormIoDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormIoDemoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormIoDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
