import { Component, OnInit } from '@angular/core';
import { FormioForm } from 'angular-formio';
import { ApiserviceService } from 'src/app/utils/services/apiservice.service';


// interface FeedbackChangeEvent {
//   data: any;
// }

@Component({
  selector: 'app-form-io-demo',
  templateUrl: './form-io-demo.component.html',
  styleUrls: ['./form-io-demo.component.css']
})
export class FormIoDemoComponent implements OnInit {
  public formBuilder : any;

  public formJSON: FormioForm = { display: 'form', components: [] };

  constructor(private apiService: ApiserviceService) { }

  ngOnInit(): void {
  }

  handleFormChange(event? : any) : void{
  debugger;
   console.log(event.form);
  }

  onFormBuilderReady(builder:any) {
    debugger;
    this.formBuilder = builder;
  }

  getFormJson()
  {
    
    // debugger;
    // if(this.formBuilder)
    //   {
    //     const jsonVar = this.formBuilder.instance.schema;
    //     console.log(jsonVar);
    //     return jsonVar;
    //   }
    //   return {};

    const jsonVar = this.formJSON.components;
    //console.log(JSON.stringify(jsonVar));

    const createdForm = JSON.stringify(jsonVar);
    this.apiService.saveFormData('Anjali',createdForm).subscribe(response => {
      console.log('Form data saved successfully:', response);
    }, error => {
      console.error('Error saving form data:', error);
    });



  }
}

  // formJSON: FormioForm = { display: 'form', components: [] };
  // userResponses: { data: any } = { data: null };
  // readOnly: boolean = false;
  
  // onFeedbackChange(event: FeedbackChangeEvent) {
  //   this.userResponses = { data: event.data };
  // }

  // handleNextClick = (e: any) => {
  //   console.log(e);
  // };

  // handlePrevClick = (e: any) => {
  //   console.log(e);
  // };

  // onSubmit(event: any) {
  //   // Handle form submission logic here
  //   console.log('Form submitted:', event);
  // }

