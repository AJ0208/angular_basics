import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login/login.component';
import { AppComponent } from './app.component';
import { AddContactComponent } from './pages/contact/add-contact/add-contact.component';
import { ContactListComponent } from './pages/contacList/contact-list/ContactListComponent';
import { DBSwitchComponent } from './dbswitch/dbswitch.component';
import { FormIoDemoComponent } from './test- FormIO/form-io-demo/form-io-demo.component';
import { FormIoRedirectionComponent } from './test- FormIO/form-io-redirection/form-io-redirection.component';
const routes: Routes = [
  {
    path : '',
    component: AppComponent
  },
  {
    path : 'login',
    component : LoginComponent
  },
  {
    path : 'addContact',
    component : AddContactComponent
  },
  {
    path:'contactList' ,
    component : ContactListComponent
  },
  {
    path:'switchDb' ,
    component : DBSwitchComponent
  },
  {
    path:'formIo' ,
    component : FormIoDemoComponent
  },
  {
    path:'formRedirect' ,
    component : FormIoRedirectionComponent
  },
  {
    path:'welcome' ,
    component : LoginComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
