import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  private loginHeader = new HttpHeaders({
    'content-type':  'application/json'
  });

  constructor(private http: HttpClient) { }

  login(username: string, password: string)
  {
    
    const data = {
      email: username,
      password: password
    }
    // wrong method
    // return this.http.post<any>(`${environment.apiUrl}/api/account/login`, 'Email='+username+'&Password='+password,{ headers: this.loginHeader });
    //for fromQuery
    // return this.http.post<any>(`${environment.apiUrl}/api/account/login?`, 'Email='+username+'&Password='+password,{ headers: this.loginHeader });
    //fromBody
    return this.http.post<any>(`${environment.apiUrl}/api/account/login`, data, { headers: this.loginHeader });
  }
  
}
