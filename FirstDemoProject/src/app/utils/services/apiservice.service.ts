import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {  HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  
  private formIoHeader = new HttpHeaders({
    'content-type':  'application/json'
  });


  constructor(private http: HttpClient) { }

  getDbNames(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/api/Db/DbDDLgetlist?Type=DbName`);
  }

  
  getUserData(): Observable<any> {
    const headerDict = {
      'Id': String(localStorage.getItem('DbId')),
    }
    const httpOptions = {
      headers: new HttpHeaders(headerDict) // Set headers provided by the component
    };

    const data = {
      Id : 'ADE038AF-0813-4802-A2C5-D4F66425C6EB'
    } 
    return this.http.post<any>(`${environment.apiUrl}/api/Db/userGet` , data , httpOptions );
  }

  saveFormData(formName: string, createdForm: string) {  
    const formData = {
        formName: formName,
        createdForm: createdForm
    };
    return this.http.post(`${environment.apiUrl}/api/FormIo/addForm`, formData, { headers: this.formIoHeader }); 
}

getForms() {
  return this.http.get<any>(`${environment.apiUrl}/api/FormIo/formdetails`);
}



}



