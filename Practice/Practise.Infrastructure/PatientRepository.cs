﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Practise.core.Common;
using Practise.core.Patient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.Infrastructure
{
    public class PatientRepository : IPatientRepository
    {
        private readonly IConfiguration _configuration;

        private static string con = string.Empty;
        public PatientRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            con = _configuration["ConnectionStrings:PracticeDemo"]!;
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(con);
            }
        }

        public async Task<Response> PatientSaveAsync(PatienSave patienSave)
        {
            Response? response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspSavePatient]", patienSave);
            return response!;
        }

        public async Task<Response> PatientUpdateAsync(PatientUpdate patientUpdate)
        {
            Response? response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspUpdatePatient]", patientUpdate);
            return response!;
        }

        public  async Task<Response> PatientDeleteAsync(PatientDelete patientDelete)
        {
            Response? response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspDeletePatient]", patientDelete);
            return response!;

        }

        public async Task<Response<Patient>> PatientGetAsync(PatientGetById patientGetById)
        {
            Response<Patient> response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[dbo].[uspGetPatient]", patientGetById);
            response = result.Read<Response<Patient>>().FirstOrDefault()!;
            response.Data = response.Status ? result.Read<Patient>().ToList() : default;
            return response;

        }

        public async Task<Response<Patient>> PatientGetListAsync(PatientGetList patientGetList)
        {
            Response<Patient> response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[dbo].[uspGetPatientList]" , patientGetList);
            response = result.Read<Response<Patient>>().FirstOrDefault()!;
            response.Data = response.Status ? result.Read<Patient>().ToList() : default;
            return response;
        }
    }
}
