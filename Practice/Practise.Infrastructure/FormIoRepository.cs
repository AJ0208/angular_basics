﻿using Microsoft.Extensions.Configuration;
using Practise.core.Common;
using Practise.core.FormIo;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Practise.core.Patient;

namespace Practise.Infrastructure
{
    public class FormIoRepository : IFormIoRepository
    {

        private readonly IConfiguration _configuration;

        private static string con = string.Empty;
        public FormIoRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            con = _configuration["ConnectionStrings:PracticeDemo"]!;

        }
        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(con);
            }
        }

        public async Task<Response> FormAdd(DtoForm dtoForm)
        {
            Response? response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspAddForm]", dtoForm);
            return response!;
        }
    }
}
