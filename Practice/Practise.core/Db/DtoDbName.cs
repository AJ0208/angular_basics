﻿using Practise.core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.core.Db
{
    public class DbGetRequest
    {
        public int Id { get; set; }
    }

    public class DbGetResponse
    {
        //public int Id { get; set; }
        //public Guid DbGuid { get; set; }
        public string? DbName { get; set; }
    }

    public class UserGetRequest
    {
        public Guid? Id { get; set; }
        //public int DbId { get; set; }
    }

    public class UserGetResponse
    {
        public Guid Id { get; set; }
        public string?  FirstName {  get; set; }
        public string?LastName { get; set; }
    }

    public class ResponseNew<T> : Response
    {

        public T? Data { get; set; }
        public bool IsSuccessStatusCode { get; set; }

    }

    public class DbGetDllResponse
    {
        public int Id { get; set; }
        public Guid DbGuid { get; set; }
        public string? DbName { get; set; }
    }

    public class DbGetDllRequest
    {
        public string? Type { get; set; }
    }

}
