﻿using Practise.core.Common;
using Practise.core.Patient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.core.Db
{
    public interface IDbRepositiry
    {
        Task<ResponseNew<DbGetResponse>> DbGetAsync(DbGetRequest dbGetRequestId);

        Task<List<DbGetDllResponse>> DbDDLgetlist(DbGetDllRequest dbGetDll);
        Task<List<UserGetResponse>> UserGetAsync(UserGetRequest userGetRequest);


        //Task<ResponseNew<DbGetResponse>> DbGetAsync(DbGetRequest  dbGetRequestId);
        //Task<Response<UserGetResponse>> UserGetAsync(UserGetRequest userGetRequest , string dbName);
    }
}
