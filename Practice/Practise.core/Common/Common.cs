﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.core.Common
{
    public class Response
    {
        public bool Status { get; set; }
        public string? Message { get; set; }

    }

    public class Response<T> : Response
    {
       
        public object? Data { get; set; }
        public bool IsSuccessStatusCode { get; set; }
        
    }

    
}
