﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.core.DapperHelperEntity
{
    public class DapperHelper
    { 
        public bool IsMasterDb { get; set; } = true;
        public DbIdGetResponse? DbResponse { get; set; }

    }

    public class DbIdGetResponse
    {
        public int Id { get; set; }
        public Guid dbguid { get; set; }
        public string? DbName { get; set; }
    }

}
