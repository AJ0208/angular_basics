﻿using Practise.core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.core.Patient
{
    public interface IPatientRepository
    {
        Task<Response> PatientSaveAsync (PatienSave patienSave);
        Task<Response> PatientUpdateAsync(PatientUpdate  patientUpdate);
        Task<Response> PatientDeleteAsync(PatientDelete patientDelete);
        Task<Response<Patient>> PatientGetAsync(PatientGetById patientGetById);
        Task<Response<Patient>> PatientGetListAsync(PatientGetList patientGetList);


    }
}
