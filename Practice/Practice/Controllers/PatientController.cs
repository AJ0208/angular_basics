﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Practise.core.Common;
using Practise.core.Patient;

namespace Practice.Controllers
{
    [Route("api/patient")]
    //[ApiController]
    public class PatientController : ControllerBase
    {
        private readonly IPatientRepository _patient;
        public PatientController(IPatientRepository patient) 
        {
            _patient = patient;
        }

        private async Task<ActionResult<T>> HandleInvalidModelState<T>(Func<Task<T>> action)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    Status = false,
                    Message = string.Join(Environment.NewLine, ModelState.Values
                                            .SelectMany(x => x.Errors)
                                            .Select(x => x.ErrorMessage)),

                });
            }

            T response = await action();

            if (response == null)
            {
                return NotFound();
            }

            return Ok(response);
        }


        private async Task<ActionResult<T>> HandleException<T>(Func<Task<T>> action)
        {
            try
            {
                return await HandleInvalidModelState(action);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpPost, Route("add")]
        public async Task<IActionResult> PatientSaveAsync([FromBody] PatienSave patienSave)
        {
           try
           {
                var response = await _patient.PatientSaveAsync(patienSave);
                return Ok(response);
           }
           catch (Exception ex)
           {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
           }

        }

        [HttpPost, Route("update")]
        public async Task<IActionResult> PatientUpdateAsync([FromBody] PatientUpdate patientUpdate)
        {
            try
            {
                var response = await _patient.PatientUpdateAsync(patientUpdate);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost, Route("delete")]
        public async Task<IActionResult> PatientDeleteAsync([FromBody] PatientDelete patientDelete)
        {
            try
            {
                var response = await _patient.PatientDeleteAsync(patientDelete);
                return Ok(response);
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost, Route("patientget")]
        public async Task<IActionResult> PatientGetAsync([FromBody] PatientGetById patientGetById)
        {
            try
            {
                var response = await _patient.PatientGetAsync(patientGetById);
                return Ok(response);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost, Route("patientdetails")]
        public async Task<IActionResult> PatientGetListAsync( [FromBody] PatientGetList patientGetList)
        {
            try
            {
                var response = await _patient.PatientGetListAsync(patientGetList);
                return Ok(response);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

    }
}
