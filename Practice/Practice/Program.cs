using Practise.core.Db;
using Practise.core.FormIo;
using Practise.core.Patient;
using Practise.Infrastructure;



var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowSpecificOrigin",
        policyBuilder => policyBuilder.AllowAnyOrigin()
            //.WithOrigins("http://localhost:4200") // Specify the client app's origin
            .AllowAnyMethod()
            .AllowAnyHeader()
            //.AllowCredentials()
            ); // Enable credentials
});

builder.Services.AddScoped<IPatientRepository, PatientRepository>();
builder.Services.AddScoped<IDbRepositiry, DbRepositiry>();
builder.Services.AddScoped<DapperHelperRepo>();
builder.Services.AddScoped<IFormIoRepository , FormIoRepository> ();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors("AllowSpecificOrigin");

app.UseAuthorization();

app.MapControllers();

app.Run();
